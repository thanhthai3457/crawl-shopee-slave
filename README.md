# Crawl-shopee-slave

## npm start
Start crawl and send data to server for data saving

## npm run pm2-shopee
Start crawl and save it into database automatically


## npm run pm2-shopeeFan
Start crawl history data from shopeefan

## Persistent applications
We run file with pm2. After that, run "pm2 save"
Then, we run "pm2 startup", and we'll get a sudo command. Just copy and run it.

$ pm2 startup
[PM2] You have to run this command as root. Execute the following command:
  sudo su -c "env PATH=$PATH:/home/unitech/.nvm/versions/node/v14.3/bin pm2 startup <distribution> -u <user> --hp <home-path>

Now PM2 will automatically restart at boot.

If you want to stop auto restart when reboot.
Just do it again but with unstartup command

$ pm2 unstartup
[PM2] To unsetup the Startup Script, copy/paste the following command:
  sudo su -c "env PATH=$PATH:/home/unitech/.nvm/versions/node/v14.3/bin pm2 unstartup <distribution> -u <user> --hp <home-path>