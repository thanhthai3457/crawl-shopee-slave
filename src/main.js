/* eslint-disable @typescript-eslint/no-var-requires */
'use strict'
const axios = require('axios')
const fs = require('fs')
const exec = require('child_process').exec

const HOST = 'https://datashopee.hotato.vn/v1'
// const HOST = 'http://localhost:3001/v1'

// create instance axios
const axiosIntance = axios.create({
  baseURL: HOST
})

const sleep = (ms = 1000) =>
  new Promise((resolve) => {
    ms !== 1000 && console.log('-----------', 'Sleep for ', ms, ' seconds', '-----------')
    setTimeout(resolve, ms)
  })

const cate_api = '/categories/get_need_update_category'
const updateCrashCate = '/categories/crashCategory'
const finishCate = '/categories/finishCategory'
const save_items_api = '/itemShopee/crawl_items'
const getProxyAPI = '/proxies/getProxy'
const shopeeLocation = 'https://shopee.vn/api/v4/search/location_filter'

let globalCatId

const referer = 'https://shopee.vn/'

const config = {
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    cookie: '',
    'x-api-source': 'pc',
    'x-shopee-language': 'vi',
    referer,
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
  },
}

const serverConfig = {
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    "Accept": "application/json, text/plain, */*"
  },
  auth: {
    username: 'hotato',
    password: 'Muaha2016!@#'
  }
}

const writeVersion = (obj) => {
  exec('git stash;')
  exec('git checkout main;')
  exec('git pull;')
  exec('npm install;pm2 flush;npm run start:pm2;pm2 restart all')
}

const getEachItem = (item) => {
  const result = {
    item: {},
    item_price: {}
  }
  const { itemid, shopid, item_basic: data } = item

  result.item = {
    itemId: itemid,
    ...data,
    name: data?.name || '',
    shopId: shopid,
    catId: data?.catid || 0,
    categoryId: globalCatId,
    cTime: data?.ctime || 0,
    historicalSold: data?.historical_sold || 0,
    likedCount: data?.liked_count || 0,
    cmtCount: data?.cmt_count || 0
  }

  result.item_price = {
    itemId: itemid,
    shopId: shopid,
    models: data?.models || [],
    price: data?.price || 0,
    priceMax: data?.price_max || 0,
    priceMin: data?.price_min || 0,
    rawDiscount: data?.raw_discount || 0,
    likedCount: data?.liked_count || 0,
    sold: data?.sold || 0,
    historicalSold: data?.historical_sold || 0,
    daySales: 0,
    stock: data?.stock || 0,
    cmtCount: data?.cmt_count || 0,
    itemRating: data?.item_rating?.rating_count || [0, 0, 0, 0, 0, 0]
  }

  return result
}

const getProxy = async () => {
  const proxyData = await axiosIntance.get(
    getProxyAPI,
    {
      ...serverConfig,
      timeout: 5000
    }
  )
  const proxy = proxyData?.data?.data?.detail

  if (!proxy) return false

  const [host, port, username, password] = proxy?.split(':')

  return { host, port, auth: { username, password } }
}

const getItems = async (url) => {
  const proxy = await getProxy()
  console.log('proxy', proxy)

  let response

  try {
    response = await axios.get(
      url,
      {
        ...config,
        proxy
      }
    )
  } catch (error) {
    response = error?.response
  }

  console.log({
    status: response?.status,
    data: response?.data?.items?.length,
    url
  })

  const newCookie = response?.headers?.['set-cookie']?.join(';')
  if (newCookie) config.headers.cookie = newCookie

  if (!config.headers.cookie) {
    const newCookie = response?.headers?.['set-cookie']?.join(';')

    if (newCookie) config.headers.cookie = newCookie

    return getItems(url)
  }

  if (response?.status === 403) {
    config.headers.cookie = ''

    console.log({ url })

    return getItems(url)
  }
  return response?.data
}

const crawShopee = async (count, locations = null) => {
  let newest = 0
  let searchResponse

  const limit = 60
  const encodeLocations = locations ? `&locations=${encodeURI(locations)}` : ''

  do {
    const apiUrl = `https://shopee.vn/api/v4/search/search_items?by=relevancy&limit=${limit}&categoryids=${globalCatId}&newest=${newest}&order=desc&page_type=search&scenario=PAGE_OTHERS&version=2${encodeLocations}`
    console.log('apiUrl', apiUrl)
    searchResponse = await getItems(apiUrl)

    if (searchResponse?.items?.length) {
      const { items } = searchResponse
      const itemObj = (items || []).reduce((res, it) => {
        if (
          !res[`${it.itemid}-${it.shopid}`]
          || res?.[`${it.itemid}-${it.shopid}`].item_basic.ctime < it.item_basic.ctime
        ) {
          res[`${it.itemid}-${it.shopid}`] = {
            itemid: it.itemid,
            shopid: it.shopid,
            item_basic: it.item_basic
          }
        }
        return res
      }, {})

      const itemArr = Object.values(itemObj)
      const getItems = itemArr.map(ite => getEachItem(ite)).filter(Boolean)
      count.count += getItems.length

      console.log('🚀  Get items', count.count)

      if (getItems?.length) {
        axiosIntance.post(
          save_items_api,
          {
            allItems: getItems,
            catId: globalCatId
          },
          serverConfig
        )
      }
    }

    newest += limit
  } while(searchResponse?.items?.length)
}

const cron_AutoCrawlItemShopee = async () => {
  try {
    const [category, locationsFilter] = await Promise.all([
      axiosIntance.get(
        cate_api,
        serverConfig
      ),
      getItems(shopeeLocation)
    ])

    let locations = []
    if (locationsFilter?.data?.length) {
      locationsFilter.data.forEach(loca => {
        locations.push(loca?.locations || [])
      })
    }
    locations = locations.flat()

    if (category.status !== 200)
      throw new Error(`Fail when get category, category status: ${category?.status}`)
    if (!category?.data?.data?.categoryId) {
      console.log('All cates was updating')
      return
    }

    const version = category.data.data.codeVersion
    const readText = fs.readFileSync(__dirname + '/version.json', 'utf8')
    const obj = JSON.parse(readText)

    if (obj.CODE_VERSION !== version) {
      writeVersion({
        CODE_VERSION: version
      })
    }

    globalCatId = Number(category.data.data.categoryId)

    let count = {
      count: 0
    }

    if (locations?.length) {
      for (const loca of locations) {
        console.log('Crawl items of: ', globalCatId, category.data.data?.displayName || '', loca)
        await crawShopee(count, loca)
        await sleep()
      }
    } else {
      await crawShopee(count)
    }
    await sleep(5000)
  } catch (error) {
    console.log('Crash when craw category:', globalCatId)
    axiosIntance.patch(
      updateCrashCate,
      {
        _id: globalCatId
      },
      serverConfig
    )
    console.log('🚀  Error', error)
    globalCatId = null
  }
}

const main = async () => {
  console.log('\n\n🚀  Ready for crawling')
  await cron_AutoCrawlItemShopee()
  if (globalCatId) {
    await axiosIntance.patch(
      finishCate,
      {
        _id: globalCatId
      },
      serverConfig
    )
  }
  console.log('🚀  Done')
}

(async () => {
  await main()
  exec('pm2 flush')
  exec('rm ~/.pm2/pm2.log')
})();
