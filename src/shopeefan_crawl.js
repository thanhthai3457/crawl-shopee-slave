/* eslint-disable @typescript-eslint/no-var-requires */
"use strict"
const axios = require("axios")
const moment = require("moment")
const exec = require("child_process").exec
const HOST = 'https://datashopee.hotato.vn/v1'
// const HOST = 'http://localhost:3001/v1'
const axiosIntance = axios.create({
  baseURL: HOST
})

const get_items_api = '/itemShopee/get_items_crawl_shopeeFan'
const save_histories_api = '/itemShopee/save_histories_api'
const getProxyAPI = '/proxies/getProxy'

const month = 3

const serverConfig = {
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    "Accept": "application/json, text/plain, */*"
  },
}

const sleep = (ms = 1000) =>
  new Promise((resolve) => {
    console.log("-----------", "Sleep for ", ms, " seconds", "-----------");
    setTimeout(resolve, ms);
  })

const genSearchParamsString = (obj) => {
  const searchParams = new URLSearchParams()

  Object.entries(obj).forEach(([key, val]) => {
    if (val !== null && val !== undefined)
      searchParams.append(key, String(val))
  })

  return searchParams.toString()
}

const getProxy = async () => {
  const proxyData = await axiosIntance.get(
    getProxyAPI,
    {
      ...serverConfig,
      timeout: 2000
    }
  )
  const proxy = proxyData?.data?.data?.detail

  if (!proxy) return false

  const [host, port, username, password] = proxy?.split(':')

  return { host, port, auth: { username, password } }
}

const getHistoryFromSPFan = async (itemId, shopId, uploadTimeLong) => {
  const proxy = await getProxy()

  const loginResponse = await axiosIntance.post(
    "https://api.keyouyun.com/auth/login",
    { username: 'hoc@hotcom.vn', password: 'longgiang' },
    {
      headers: {
        Host: "api.keyouyun.com",
        authority: "api.keyouyun.com",
        "sec-ch-ua-mobile": "?0",
        "content-type": "application/json;charset=UTF-8",
        "eagleeye-sessionid": "zvlyF1jweOtxOFp1gtUXu11mjCyO",
        accept: "application/json, text/plain",
        "x-erp-language": "en-US",
        "eagleeye-pappname": "e8e8o1egv9@2f06a7c36718eff",
        "x-erp-type": "erp",
        "eagleeye-traceid": "fae0d17d1648727389436105218eff",
        "x-request-id": "nccpul2jo0wcrcms",
        "sec-ch-ua-platform": "macOS",
        origin: "https://erp.keyouyun.com",
        "sec-fetch-site": "same - site",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "accept-language":
          "vi-VN,vi;q=0.9,en-US;q=0.8,en;q=0.7,fr-FR;q=0.6,fr;q=0.5",
        cookie:
          "_ga=GA1.2.235353260.1640011023; _ati=4964970118914; _gid=GA1.2.1485922790.1648692279; NG_TRANSLATE_LANG_KEY=%22vi%22; _gat_gtag_UA_158792964_1=1",
      },
      proxy
    }
  )
  const cookie = `_ga=GA1.2.235353260.1640011023; _ati=4964970118914; _gid=GA1.2.1485922790.1648692279; NG_TRANSLATE_LANG_KEY=%22vi%22; _gat_gtag_UA_158792964_1=1; access_token=${loginResponse.data.access_token}`


  let endDateTime = moment().subtract(1, "day").endOf("day")
  let beginDateTime = moment().subtract(month, "month")

  endDateTime = moment(endDateTime)
    .toISOString()
    .replace("T", " ")
    .split(".")[0]
  beginDateTime = moment(beginDateTime)
    .toISOString()
    .replace("T", " ")
    .split(".")[0]

  const history = await axiosIntance.get(
    `https://world.keyouyun.com/yasuo/api/historical-dynamic-prices-sales?${genSearchParamsString(
      {
        itemId,
        shopId,
        beginDateTime: beginDateTime,
        endDateTime: endDateTime,
        platformId: 10,
        stationId: 87,
        uploadTimeLong,
      }
    )}`,
    {
      headers: {
        cookie,
        accept: "application/json",
        "accept-language":
          "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5",
      },
      proxy
    },
  )

  const { data } = history?.data?.data || []

  const result = (data || []).map((elem) => {
    let priceTime = moment(elem?.gmtDate, "YYYY-MM-DD").startOf("day").valueOf()
    if (!elem.updateTime) {
      const tempTime = new Date(elem.updateTime)
      priceTime = tempTime.getTime()
    }
    const temp = {
      priceTime,
      updateTime: elem?.updateTime,
      priceMax: elem?.maxPrice || 0,
      priceMin: elem?.minPrice || 0,
      daySales: elem?.daySales || 0,
      historicalSold: elem?.allSales || 0,
    }
    return temp
  })

  const final = {}

  result.forEach((elem) => {
    let temp = elem
    if (final?.[elem.priceTime]) {
      temp =
        moment(final[elem.priceTime].updateTime) > moment(elem.updateTime)
          ? final[elem.priceTime]
          : elem
    }
    final[elem.priceTime] = temp
  })

  return {
    result: Object.values(final),
    itemId,
    shopId,
  }
}

const main = async () => {
  try {
    console.log("\n🚀  Ready for crawling")

    const getItems = await axiosIntance.get(
      get_items_api,
      serverConfig
    )

    if (getItems.status !== 200) {
      throw new Error('Cannot get category')
    }

    const items = getItems.data?.items

    console.log(`===== 🚀  Crawl ${month} month${month > 1 ? 's' : ''} of ${items.length} items =====`)

    const fetchHistories = await Promise.all(
      (items || []).map((item) =>
        getHistoryFromSPFan(item.itemId, item.shopId, item.cTime)
      )
    )

    const requestArr = fetchHistories.filter(Boolean)

    const allElem = requestArr.flat()

    await axiosIntance.post(
      save_histories_api,
      allElem,
      serverConfig
    )

    console.log("🚀  Done ===== close db connection =====\n")
    await sleep(5000)
  } catch (error) {
    console.log("error", error)
    throw error
  }
};

(async () => {
  await main();
  exec("pm2 flush");
  exec("rm ~/.pm2/pm2.log");
})();
