/* eslint-disable @typescript-eslint/no-var-requires */
'use strict'
const axios = require('axios')
const moment = require('moment')
const fs = require('fs')
const md5 = require('md5')
const exec = require('child_process').exec

const HOST = 'https://datashopee.hotato.vn/v1'

// create instance axios
const axiosIntance = axios.create({
  baseURL: HOST
})

const cate_api = '/itemShopee/get_need_update_category'
const save_items_api = '/itemShopee/crawl_items'

const referer = 'https://shopee.vn/'
const cookie = 'SPC_F=ri9EVtwRs5ozCOezsGLjP6XDk9XWiorR; REC_T_ID=01b9a416-4e12-11ec-b023-48df37df8639; csrftoken=TqlteoNmwJEy0kXxf1Q6G8qSli29T7oH; SC_DFP=1grvuS6qmrbjPuhoFMkZogPqKf1RDGw8; welcomePkgShown=true; _hjSessionUser_868286=eyJpZCI6ImQ5YmIzZTQ0LTFmN2ItNWJhZi04NDRmLTdhN2VjMjVlZWRhOSIsImNyZWF0ZWQiOjE2Mzc4NTk5ODAyNTUsImV4aXN0aW5nIjp0cnVlfQ==; _QPWSDCXHZQA=f77a8bfe-5a03-4e7a-8f04-b7b33056bac5; G_ENABLED_IDPS=google; G_AUTHUSER_H=0; SPC_CLIENTID=cmk5RVZ0d1JzNW96agkraqwjtaenkzan; UYOMAPJWEMDGJ=; SC_SSO=-; SPC_WST="dLHHMsCR+/4YtuuEB4OE2xSHvClOe4CvL5X7TOPF2laphh1lKP7anFr1Tdw1DMLy2qzQHzaiWYnOQ/m/6IiFwWapWNEPymqElJa+qsBuG68UaeWjku9pRnGHlW7x2MoVBazIWGp5nw1oUYqwjgFdIr1iXfDe+6hZu74MC2qxTRk="; SC_SSO_U=-; SPC_SC_SA_TK=; SPC_SC_SA_UD=; _ga_VYF4T4BCNH=GS1.1.1641574347.2.1.1641574958.0; _gcl_au=1.1.230913930.1645707851; __stripe_mid=60e92d00-22e5-45a7-8d61-e74aa61d158cef61b4; spc_ckt_reqid=1f8a1b84da1a10fa92e36f86de0e0400:010001a720d4beb7:0000002a84ebe28d; __LOCALE__null=VN; _gid=GA1.2.1472665620.1648531936; _gcl_aw=GCL.1648549131.Cj0KCQjw3IqSBhCoARIsAMBkTb2XcshuDpnM-0jD3sNjuBX8FmHAjxRlnx0DIlMmTKJjHfibHBlSt-gaAhwoEALw_wcB; _gac_UA-61914164-6=1.1648549133.Cj0KCQjw3IqSBhCoARIsAMBkTb2XcshuDpnM-0jD3sNjuBX8FmHAjxRlnx0DIlMmTKJjHfibHBlSt-gaAhwoEALw_wcB; SPC_EC=V2ZPd3BuMTU2NmJMRjVlczT0ZFR1IgRPyJODBblKnMvHxrn7t5V8J1AlgXSDr69fYDGs0iaF8M6dmaswk0929LAoAOs28ORRW9s7eX24SzYQItaOQoeK3JPggmwmV+1osSN4xPIpv87QibllPBrR8uapZZNrRkdC9wcMiO1HOM8=; SPC_U=20034500; SPC_IA=1; _med=affiliates; AMP_TOKEN=%24NOT_FOUND; _hjSession_868286=eyJpZCI6Ijc4MDY0ZmNiLTRjM2EtNDBiOS1iNmE1LTBlZDE3YTk3M2Q4YSIsImNyZWF0ZWQiOjE2NDg4NjYyODI2NjcsImluU2FtcGxlIjpmYWxzZX0=; _hjAbsoluteSessionInProgress=1; SPC_T_IV="visv/n3L3Q4bfXfh3/e/wA=="; SPC_T_ID="wCf0skg1hLK+ijiJkdGe7fcJOGQF27BWWv9/FIU2tRFmFgrfm1Ll921tQomFwVJye4fPy0nnQMnnP2+NJ91vK2kktd7ph8eSMRsupyMGykg="; SPC_SI=QFE8YgAAAABHUW14MTg3McCdPQAAAAAATGlhUXRMak8=; SPC_R_T_ID=wCf0skg1hLK+ijiJkdGe7fcJOGQF27BWWv9/FIU2tRFmFgrfm1Ll921tQomFwVJye4fPy0nnQMnnP2+NJ91vK2kktd7ph8eSMRsupyMGykg=; SPC_R_T_IV=visv/n3L3Q4bfXfh3/e/wA==; SPC_T_ID=wCf0skg1hLK+ijiJkdGe7fcJOGQF27BWWv9/FIU2tRFmFgrfm1Ll921tQomFwVJye4fPy0nnQMnnP2+NJ91vK2kktd7ph8eSMRsupyMGykg=; SPC_T_IV=visv/n3L3Q4bfXfh3/e/wA==; _ga_M32T05RVZT=GS1.1.1648866277.333.1.1648867020.60; _ga=GA1.2.1289269113.1637859980; _dc_gtm_UA-61914164-6=1; cto_bundle=TxuoVF8xblRxbklDRDV0N1FSdHNjMmlRNVYxd3VUb1A1bEhqR1UyV3czbkZsWEFMUlo3WmpNM1RrYlp2akNzMWw0N0JpMnpFaHJ4Z2hVRElTYlh0TXZ3JTJGUUwyVFN1ZTV3cDNTU3dWSzdxTDFwUVNiREdES2RRQkVsMHpUa252eWJveVpMamt2RVNBRThkMHRHV1p3c2JDdUoyckIyeDM3aHRoZkE1ZmZyUHhoJTJCWnJRZGtWd2dpeERkdjdEJTJCMXluSXliOXk; SPC_ST=.YjBjZmJHVFpCRjk5ZzhOQmPivvq0YXP6ZiaC+2lQjOaFWPZe6XubbKldLyYU1TzsJBGB6n/04Y6J0bRlrPexriCBSnspMLcqMMqosO6MqnFBEA/eK01J4oJTyUJ3jh1WyaqEtJLDiQS23GzcYs9WWhuHUWZcnmMyJVPKZSGJ68v9YTiU3Y1rchlGBr0xslUUiKnpL2gECLdtPRx6cNuw9w==; shopee_webUnique_ccd=hOA5vz4RfuKRuiFHSvRuiQ%3D%3D%7C9egNXJ1KSUxfRucLUlDuqZXAWbsu4MuZ0GC6S3Y7Uw%2BbafrkP0W0CcIhbXtXXR6INSs3VHaxhUmoIv8gTj1ftAM%3D%7CXNlnn1gxNeHBKQhT%7C04%7C3'

const config = {
  headers: {
    'x-api-source': 'pc',
    'x-shopee-language': 'vi',
    'sec-ch-ua': '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'x-requested-with': 'XMLHttpRequest',
    accept: 'application/json',
    'content-type': 'application/json',
    'user-agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.83 Safari/537.36',
    'x-api-source': 'pc',
    'x-shopee-language': 'vi',
    referer,
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'accept-language': 'en-US,en;q=0.9,vi;q=0.8'
  }
}

const serverConfig = {
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
    "Accept": "application/json, text/plain, */*"
  },
}

const writeVersion = (obj) => {
  exec('git stash;')
  exec('git checkout main;')
  exec('git pull;')
  exec('npm install;pm2 flush;pm2 restart all')
}

const sleep = (ms = 5000) =>
  new Promise((resolve) => {
    ms !== 5000 && console.log('-----------', 'Sleep for ', ms, ' seconds', '-----------')
    setTimeout(resolve, ms)
  })

const getEachItem = (item, categoryIds) => {
  const result = {
    item: {},
    item_price: {}
  }
  const { itemid, shopid, item_basic: data } = item

  result.item = {
    itemId: itemid,
    ...data,
    name: data?.name || '',
    shopId: shopid,
    catId: data?.catid || 0,
    categoryId: Number(categoryIds),
    cTime: data?.ctime || 0
  }

  result.item_price = {
    itemId: itemid,
    shopId: shopid,
    price: data?.price || 0,
    priceBeforeDiscount: data?.price_before_discount || 0,
    priceMax: data?.price_max || 0,
    priceMaxBeforeDiscount: data?.price_max_before_discount || 0,
    priceMin: data?.price_min || 0,
    priceMinBeforeDiscount: data?.price_min_before_discount || 0,
    rawDiscount: data?.raw_discount || 0,
    likedCount: data?.liked_count || 0,
    sold: data?.sold || 0,
    historicalSold: data?.historical_sold || 0,
    daySales: 0,
    stock: data?.stock || 0,
    createdAt: moment().valueOf(),
    updatedAt: moment().valueOf()
  }

  return result
}

const cron_AutoCrawlItemShopee = async () => {
  console.log('Start crawl shopee items')
  await sleep()

  try {
    const category = await axiosIntance.get(
      cate_api,
      serverConfig
    )

    if (category.status !== 200 || !category?.data?.categoryId) {
      throw new Error('Cannot get category')
    }

    const version = category.data.codeVersion
    const readText = fs.readFileSync(__dirname + '/version.json', 'utf8')
    const obj = JSON.parse(readText)

    if (obj.CODE_VERSION !== version) {
      writeVersion({
        CODE_VERSION: version
      })
    }

    const cate = Number(category.data.categoryId)
    console.log('Crawl item of category: ', cate)
    const limit = 60

    let keepFetching = true
    let newest = 0
    let count = 0

    while (keepFetching) {
      let apiUrl = `https://shopee.vn/api/v4/search/search_items?by=relevancy&limit=${limit}&match_id=${cate}&newest=${newest}&order=desc&page_type=search&scenario=PAGE_OTHERS&version=2`
      apiUrl = encodeURI(apiUrl)
      const str_request = `55b03${md5(apiUrl)}55b03`;
      const if_none_match = `55b03-${md5(str_request)}`;

      config['if-none-match'] = if_none_match
      config.cookie = `${cookie}_${+new Date()}`

      const response = await axiosIntance.get(
        apiUrl,
        config
      )
      const { items, error, error_msg } = response.data
      if (error || error_msg || !items?.length) {
        keepFetching = false
      } else {
        const itemObj = (items || []).reduce((res, it) => {
          if (
            !res[`${it.itemid}-${it.shopid}`]
            || res?.[`${it.itemid}-${it.shopid}`].item_basic.ctime < it.item_basic.ctime
          ) {
            res[`${it.itemid}-${it.shopid}`] = {
              itemid: it.itemid,
              shopid: it.shopid,
              item_basic: it.item_basic
            }
          }
          return res
        }, {})

        const itemArr = Object.values(itemObj)

        const getItems = itemArr.map(ite => getEachItem(ite, cate)).filter(Boolean)
        count += getItems.length
        console.log('🚀  Get items', count)

        if (getItems?.length) {
          axiosIntance.post(
            save_items_api,
            {
              allItems: getItems,
              catId: cate
            },
            serverConfig
          )
        }

        newest += 60
      }

      await sleep()
    }
    keepFetching = true
    console.log('🚀  Crawl: ', newest, ' items')
    newest = 0
  } catch (error) {
    console.log('🚀  Error', error)
    await sleep(30000)
    throw error
  }
}

const main = async () => {
  console.log('\n\n🚀  Ready for crawling')
  await cron_AutoCrawlItemShopee()
  console.log('🚀  Done ===== close db connection =====')
  await sleep(30000)
}

(async () => {
  await main()
  exec('pm2 flush')
  exec('rm ~/.pm2/pm2.log')
})();
